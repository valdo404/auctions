package org.levaldo.auctions

import cats.effect.{ConcurrentEffect, ContextShift, Timer}
import fs2.Pipe
import org.levaldo.auctions.{Auction, Bid, Buyer, Loss, BuyerNotification, Win}

import scala.collection.mutable

object AuctionLogic {
  type WinningBids = mutable.Map[Buyer, Bid]
  val empty = mutable.Map.empty[Buyer, Bid]

  val priceOrdering =
    Ordering.by[(Buyer, Bid), Double] { case (_, bid) => bid.price }.reverse

  /**
    * Selects highest bids by buyer (max by buyer)
    */
  def highestBidByBuyer[O](
    auction: Auction[O]
  )(currentWinners: WinningBids)(buyer: Buyer, bid: Bid): WinningBids = {
    currentWinners.updateWith(buyer) {
      case None                                      => Some(bid)
      case Some(oldBid) if oldBid.price <= bid.price => Some(bid)
      case Some(oldBid) if oldBid.price > bid.price  => Some(oldBid)
    }

    currentWinners
  }

  /**
    * Second price logic that determines the winner, its clearing price and the losers
    *
    * @todo second price can be determined in a streaming fashion
    * @todo ordering can be done in a streaming fashion
    * @todo structure can be a treemap whose keys are bids
    */
  def secondPriceWinnerAndLosers[O](
    auction: Auction[O]
  )(highestBids: Seq[(Buyer, Bid)]): Seq[BuyerNotification] = {
    val (winners, losers) = highestBids
      .filter(_._2.price > auction.reservePrice)
      .sorted(priceOrdering)
      .splitAt(2)

    val (highestBid, secondHighest) = winners.splitAt(1)

    val secondPrice =
      secondHighest.headOption
        .map {
          case (_, bid) => bid.price
        }
        .getOrElse(auction.reservePrice)

    (losers ++ secondHighest).map {
      case (buyer, bid) => Loss(bid, buyer)
    } ++ highestBid.map {
      case (buyer, bid) =>
        Win(bid, secondPrice, buyer)
    }
  }
}

class SealedSecondPriceMarketplace[F[_]: ConcurrentEffect: Timer, O](
  buyerRegistry: BuyerRegistry[F, O],
  bidObserver: Pipe[F, (Buyer, Bid), Unit]
) extends Marketplace[F, O] {

  /**
    * @todo do not use global timeout but timeout by buyer
    */
  override def runAuction(
    auction: Auction[O]
  ): fs2.Stream[F, BuyerNotification] = {
    collectBids(auction)
      .interruptAfter(auction.timeout) // note: other strategy is possible
      .fold(AuctionLogic.empty) {
        case (winners, (buyer, bid)) =>
          AuctionLogic.highestBidByBuyer(auction)(winners)(buyer, bid)
      }
      .flatMap(highest => {
        fs2.Stream
          .emits(
            AuctionLogic.secondPriceWinnerAndLosers(auction)(highest.toSeq)
          )
      })
  }

  private def collectBids(auction: Auction[O]) = {
    buyerRegistry.buyers
      .map {
        case (buyer, endpoint) =>
          endpoint.bidOn(auction).map((buyer, _))
      }
      .fold1(_.merge(_))
      .flatten
      .observe(bidObserver)
  }
}
