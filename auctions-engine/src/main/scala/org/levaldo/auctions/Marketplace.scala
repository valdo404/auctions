package org.levaldo.auctions

import scala.concurrent.duration.FiniteDuration
import scala.language.higherKinds

case class Buyer(buyerId: String)

trait BuyerNotification {
  val bid: Bid
  val buyer: Buyer
}

case class Auction[O](obj: O, reservePrice: Double, timeout: FiniteDuration)
case class Bid(price: Double)
case class Win(bid: Bid, clearingPrice: Double, buyer: Buyer)
    extends BuyerNotification
case class Loss(bid: Bid, buyer: Buyer) extends BuyerNotification

trait BidderEndpoint[F[_], O] {
  def bidOn(auction: Auction[O]): fs2.Stream[F, Bid]
}

trait NotificationEndpoint[F[_]] {
  def notify(notification: BuyerNotification): F[Unit]
}

trait BuyerEndpoint[F[_], O]
    extends BidderEndpoint[F, O]
    with NotificationEndpoint[F]

/** @todo: also deactivate buyer */
trait BuyerRegistry[F[_], O] {
  def registerBuyer(buyer: Buyer, endpoint: BuyerEndpoint[F, O]): F[Unit]
  def buyers: fs2.Stream[F, (Buyer, BuyerEndpoint[F, O])]
}

trait Marketplace[F[_], O] {
  def runAuction(auction: Auction[O]): fs2.Stream[F, BuyerNotification]
}
