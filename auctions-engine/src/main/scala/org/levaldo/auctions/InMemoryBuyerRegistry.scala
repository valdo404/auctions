package org.levaldo.auctions

import cats.effect.Effect

import scala.collection.mutable

class InMemoryBuyerRegistry[F[_]: Effect, O] extends BuyerRegistry[F, O] {
  val F = implicitly[Effect[F]]
  val map: mutable.Map[Buyer, BuyerEndpoint[F, O]] = mutable.Map()

  override def registerBuyer(buyer: Buyer,
                             endpoint: BuyerEndpoint[F, O]): F[Unit] = {
    F.delay(map.addOne((buyer, endpoint)))
  }

  override def buyers: fs2.Stream[F, (Buyer, BuyerEndpoint[F, O])] = {
    fs2.Stream.emits(map.toSeq)
  }
}
