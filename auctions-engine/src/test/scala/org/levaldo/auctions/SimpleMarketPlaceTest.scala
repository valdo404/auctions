package org.levaldo.auctions

import cats.Show
import monix.eval.Task
import org.scalatest.{FunSuite, GivenWhenThen, Matchers}

import scala.language.postfixOps

class BuyerTestEndpoint[F[_], String](repliedBids: Seq[Bid])
    extends BuyerEndpoint[F, String] {
  override def notify(notification: BuyerNotification): F[Unit] = ???

  override def bidOn(auction: Auction[String]): fs2.Stream[F, Bid] =
    fs2.Stream.emits(repliedBids)
}

class SimpleMarketPlaceTest extends FunSuite with GivenWhenThen with Matchers {
  import cats.implicits._

  implicit val notifShow = Show.fromToString[BuyerNotification]
  implicit val bidShow = Show.fromToString[(Buyer, Bid)]

  import scala.concurrent.duration._
  import monix.execution.Scheduler.Implicits.global

  test("Marketplace should be able to launch second price auctions") {
    Given("A marketplace that is applying vickrey auction with sealed bids")
    val buyerRegistry = new InMemoryBuyerRegistry[Task, String]
    import buyerRegistry._

    Given("Some buyers in the marketplace")
    Then(
      "The buyers should tell if they are interested or not by proposing a list of bids"
    )

    (registerBuyer(Buyer("A"), new BuyerTestEndpoint(Seq(Bid(110), Bid(130)))) *>
      registerBuyer(Buyer("B"), new BuyerTestEndpoint(Seq())) *>
      registerBuyer(Buyer("C"), new BuyerTestEndpoint(Seq(Bid(125)))) *>
      registerBuyer(
        Buyer("D"),
        new BuyerTestEndpoint(Seq(Bid(105), Bid(115), Bid(90)))
      ) *>
      registerBuyer(
        Buyer("E"),
        new BuyerTestEndpoint(Seq(Bid(132), Bid(135), Bid(140)))
      )).runSyncStep

    val marketPlace = new SealedSecondPriceMarketplace(
      buyerRegistry,
      (stream: fs2.Stream[Task, (Buyer, Bid)]) => stream.showLines(Console.out)
    )

    Given("An issued auction, with a reserve price and 10 seconds expiration")
    val auction =
      Auction(obj = "patate", reservePrice = 100d, timeout = 10 seconds)

    Then(
      "After some defined time, the auction is closed and the winner or losers are notified using the second highest bid"
    )
    val winningBids =
      marketPlace.runAuction(auction)

    val notificationList: Seq[BuyerNotification] = winningBids
      .observe(_.showLines(Console.out))
      .compile
      .toList
      .runSyncUnsafe(Duration.Inf)

    notificationList should equal(
      List(
        Loss(Bid(125.0), Buyer("C")),
        Loss(Bid(115.0), Buyer("D")),
        Loss(Bid(130.0), Buyer("A")),
        Win(Bid(140.0), 130.0, Buyer("E"))
      )
    )
  }
}
