libraryDependencies += "co.fs2" %% "fs2-core" % "1.1.0-M2"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.8"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.8" % "test"
libraryDependencies += "io.monix" %% "monix" % "3.0.0"
libraryDependencies += "io.monix" %% "monix-catnap" % "3.0.0"
libraryDependencies += "io.monix" %% "monix-eval" % "3.0.0"
