libraryDependencies += "com.beachape" %% "enumeratum" % "1.5.13"
libraryDependencies += "io.grpc" % "grpc-services" % "1.23.0"
libraryDependencies += "io.grpc" % "grpc-netty" % "1.23.0"
libraryDependencies += "io.grpc" % "grpc-grpclb" % "1.23.0"
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"

dockerExposedPorts ++= Seq(9999, 10001, 10002)
