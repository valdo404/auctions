package org.levaldo.auctions

import cats.effect.{ConcurrentEffect, ContextShift, ExitCode, Timer}
import cats.{Parallel, Show}
import fs2.Pipe
import io.grpc.netty.{NegotiationType, NettyChannelBuilder, NettyServerBuilder}
import io.grpc.protobuf.services.ProtoReflectionService
import io.grpc.{Metadata, Server, ServerServiceDefinition}
import monix.eval.{Task, TaskApp}
import org.levaldo.auctions.SpecificModel.ItemMetadata
import pb.auctions.{BuyerServiceFs2Grpc, MarketplaceServiceFs2Grpc}

import scala.language.postfixOps

object AuctionApps extends TaskApp {
  override def run(args: List[String]): Task[ExitCode] = {
    new GrpcServer[Task].run()
  }
}

class GrpcServer[F[_]: ConcurrentEffect: Timer: ContextShift: Parallel] {
  implicit val notifShow = Show.fromToString[BuyerNotification]
  implicit val bidShow = Show.fromToString[(Buyer, Bid)]
  val F = implicitly[ConcurrentEffect[F]]

  import cats.effect._
  import cats.implicits._
  import org.lyranthe.fs2_grpc.java_runtime.implicits._

  val bidPipe: Pipe[F, (Buyer, Bid), Unit] = _.showLines(Console.out)

  def run(): F[ExitCode] = {
    val buyerRegistry = new InMemoryBuyerRegistry[F, ItemMetadata]

    val buyersAndPorts = List((Buyer("A"), 10000), (Buyer("B"), 10001))

    // launch a bunch of buyers and register them
    val buyers: List[F[Unit]] =
      buyersAndPorts.map {
        case (buyer, port) =>
          buyerRegistry.registerBuyer(buyer, buyerClient(port)) *>
            buyerServer(port)
      }

    // launch everything in a row
    (buyers ++ List(marketPlaceServer(buyerRegistry))).parSequence
      .as(ExitCode.Success)
  }

  private def buyerClient(buyerPort: Int) = {
    new GrpcBuyerEndpoint[F](
      NettyChannelBuilder
        .forAddress("127.0.0.1", buyerPort)
        .negotiationType(NegotiationType.PLAINTEXT)
    )
  }

  private def marketPlaceServer(
    buyerRegistry: BuyerRegistry[F, ItemMetadata]
  ) = {
    val port = 9999

    launchDiscoverableServer(
      result => F.delay(println(s"marketplace launched on $port", result)),
      port,
      MarketplaceServiceFs2Grpc.bindService(
        new GrpcMarketPlaceServer[F, Metadata](
          new SealedSecondPriceMarketplace(buyerRegistry, bidPipe)
        )
      )
    )
  }

  private def buyerServer(port: Int) = {
    launchDiscoverableServer(
      result => F.delay(println(s"buyer server launched on $port", result)),
      port,
      BuyerServiceFs2Grpc.bindService(
        new GrpcBuyerServer[F, Metadata](new QuickAndDirtyBuyerEndpoint[F]())
      )
    )
  }

  /** Launch some grpc discoverable server */
  private def launchDiscoverableServer(afterLaunch: Server => F[Unit],
                                       port: Int,
                                       service: ServerServiceDefinition) = {
    NettyServerBuilder
      .forPort(port)
      .addService(service)
      .addService(ProtoReflectionService.newInstance())
      .stream[F]
      .evalMap(server => {
        F.delay(server.start()).flatTap(afterLaunch)
      })
      .evalMap(_ => F.never)
      .compile
      .drain
  }
}
