package org.levaldo.auctions

import cats.effect.{Concurrent, ConcurrentEffect}
import cats.implicits._
import io.grpc.{ManagedChannel, Metadata}
import io.grpc.netty.NettyChannelBuilder
import org.levaldo.auctions.PbConverters._
import org.lyranthe.fs2_grpc.java_runtime.implicits._
import pb.auctions.{BuyerServiceFs2Grpc, MarketplaceServiceFs2Grpc}
import SpecificModel._

class GrpcBuyerEndpoint[F[_]: ConcurrentEffect](
  managedChannelBuilder: NettyChannelBuilder
) extends BuyerEndpoint[F, ItemMetadata] {

  private val F = implicitly[ConcurrentEffect[F]]

  private val grpcService: F[BuyerServiceFs2Grpc[F, Metadata]] =
    managedChannelBuilder.resource.allocated
      .map(tuple => BuyerServiceFs2Grpc.stub(tuple._1))

  override def notify(notification: BuyerNotification): F[Unit] = {
    grpcService.flatMap(_.notify(notification, new Metadata()) *> F.unit)
  }

  override def bidOn(auction: Auction[ItemMetadata]): fs2.Stream[F, Bid] = {
    fs2.Stream
      .eval(grpcService)
      .flatMap(_.bidOn(auction, new Metadata()))
      .map(toBid)
  }
}
