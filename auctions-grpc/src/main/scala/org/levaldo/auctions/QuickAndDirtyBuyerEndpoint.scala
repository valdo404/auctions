package org.levaldo.auctions

import cats.effect.{ConcurrentEffect, Timer}
import org.levaldo.auctions.SpecificModel.ItemMetadata

import scala.language.postfixOps
import scala.util.Random

class QuickAndDirtyBuyerEndpoint[F[_]: ConcurrentEffect: Timer]
    extends BuyerEndpoint[F, ItemMetadata] {
  import scala.concurrent.duration._

  val F = implicitly[ConcurrentEffect[F]]

  override def bidOn(auction: Auction[ItemMetadata]): fs2.Stream[F, Bid] = {
    val random = new Random()

    fs2.Stream
      .emits((0 to random.between(0, 10)))
      .covary[F]
      .delayBy(500 millis)
      .map(
        _ =>
          Bid(random.between(auction.reservePrice, auction.reservePrice * 1.5))
      )
  }

  override def notify(notification: BuyerNotification): F[Unit] = F.unit
}
