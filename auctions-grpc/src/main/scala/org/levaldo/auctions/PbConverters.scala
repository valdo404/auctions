package org.levaldo.auctions

import java.util.concurrent.TimeUnit

import scala.concurrent.duration.FiniteDuration
import scala.language.implicitConversions

/**
  * protobuf duration <-> finiteduration
  * @todo is it opensourced ?
  */
object DurationPbConverters {
  implicit def fromDuration(
    finiteDuration: FiniteDuration
  ): com.google.protobuf.duration.Duration = {
    com.google.protobuf.duration.Duration(
      finiteDuration.toSeconds,
      ((finiteDuration.toNanos - (finiteDuration.toSeconds * 1000l * 1000l * 1000l))).toInt
    )
  }
  implicit def toDuration(
    pbDuration: com.google.protobuf.duration.Duration
  ): FiniteDuration = {
    FiniteDuration.apply(
      pbDuration.seconds * 1000l * 1000l * 1000l + pbDuration.nanos,
      TimeUnit.NANOSECONDS
    )
  }
}

/**
  * Protobuf converters for conversion between the code model and the PB model
  * One should be able to remove mapping code but this one is intended on purpose.
  */
object PbConverters {
  import SpecificModel._
  import DurationPbConverters._

  // Notification: Attention should be paid to inheritance
  implicit def toNotification(
    pbNotification: pb.auctions.Notification
  ): BuyerNotification = {
    if (pbNotification.win)
      Win(
        pbNotification.bid.get,
        pbNotification.clearingPrice,
        pbNotification.buyer.get
      )
    else
      Loss(pbNotification.bid.get, pbNotification.buyer.get)

  }
  implicit def fromNotification(
    notification: BuyerNotification
  ): pb.auctions.Notification = {
    notification match {
      case w: Win =>
        pb.auctions.Notification(
          Some(w.bid),
          Some(w.buyer),
          w.clearingPrice,
          true
        )
      case l: Loss =>
        pb.auctions.Notification(Some(l.bid), Some(l.buyer))
    }
  }

  // I guess the following could be automated by some macro code
  implicit def toBid(pbBid: pb.auctions.Bid): Bid = { Bid(pbBid.price) }
  implicit def fromBid(bid: Bid): pb.auctions.Bid = {
    pb.auctions.Bid(bid.price)
  }

  implicit def toBuyer(pbBid: pb.auctions.Buyer): Buyer = {
    Buyer(pbBid.buyerId)
  }
  implicit def fromBuyer(bid: Buyer): pb.auctions.Buyer = {
    pb.auctions.Buyer(bid.buyerId)
  }

  implicit def fromAuction(
    auction: Auction[ItemMetadata]
  ): pb.auctions.Auction = {
    pb.auctions.Auction(
      Some(auction.obj),
      auction.reservePrice,
      Some(auction.timeout),
    )
  }
  implicit def toAuction(
    pbAuction: pb.auctions.Auction
  ): Auction[ItemMetadata] = {
    Auction(
      pbAuction.metadata.get,
      pbAuction.reservePrice,
      pbAuction.timeout.get
    )
  }

  implicit def fromMetadata(
    itemMetadata: ItemMetadata
  ): pb.auctions.ItemMetadata = {
    pb.auctions.ItemMetadata(itemMetadata.name, itemMetadata.itemType)
  }
  implicit def toMetadata(pbItem: pb.auctions.ItemMetadata): ItemMetadata = {
    ItemMetadata(pbItem.name, pbItem.`type`)
  }

  implicit def fromItemType(
    itemType: ItemType
  ): pb.auctions.ItemMetadata.ItemType = {
    itemType match {
      case ItemType.Book   => pb.auctions.ItemMetadata.ItemType.BOOK
      case ItemType.Camera => pb.auctions.ItemMetadata.ItemType.CAMERA
      case ItemType.Other  => pb.auctions.ItemMetadata.ItemType.OTHER
      case _               => pb.auctions.ItemMetadata.ItemType.OTHER
    }
  }
  implicit def toItemType(
    pbItemType: pb.auctions.ItemMetadata.ItemType
  ): ItemType = {
    pbItemType match {
      case pb.auctions.ItemMetadata.ItemType.BOOK   => ItemType.Book
      case pb.auctions.ItemMetadata.ItemType.CAMERA => ItemType.Camera
      case pb.auctions.ItemMetadata.ItemType.OTHER  => ItemType.Other
      case _                                        => ItemType.Other
    }
  }
}
