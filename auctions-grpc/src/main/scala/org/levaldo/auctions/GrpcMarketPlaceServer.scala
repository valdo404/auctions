package org.levaldo.auctions

import org.levaldo.auctions.SpecificModel.ItemMetadata
import pb.auctions.MarketplaceServiceFs2Grpc

class GrpcMarketPlaceServer[F[_], A](marketplace: Marketplace[F, ItemMetadata])
    extends MarketplaceServiceFs2Grpc[F, A] {
  import PbConverters._

  override def runAuction(request: pb.auctions.Auction,
                          ctx: A): fs2.Stream[F, pb.auctions.Notification] = {
    marketplace.runAuction(request).map(fromNotification)
  }
}
