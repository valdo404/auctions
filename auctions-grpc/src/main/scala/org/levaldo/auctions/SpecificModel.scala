package org.levaldo.auctions

import enumeratum.{Enum, EnumEntry}

object SpecificModel {
  sealed trait ItemType extends EnumEntry
  object ItemType extends Enum[ItemType] {
    val values = findValues

    case object Book extends ItemType
    case object Camera extends ItemType
    case object Other extends ItemType
  }

  case class ItemMetadata(name: String, itemType: ItemType)
}
