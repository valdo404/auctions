package org.levaldo.auctions

import cats.effect.ConcurrentEffect
import cats.implicits._
import io.grpc.Metadata
import io.grpc.netty.NettyChannelBuilder
import org.levaldo.auctions.PbConverters._
import org.levaldo.auctions.SpecificModel._
import org.lyranthe.fs2_grpc.java_runtime.implicits._
import pb.auctions.MarketplaceServiceFs2Grpc

class GrpcMarketplace[F[_]: ConcurrentEffect](
  managedChannelBuilder: NettyChannelBuilder
) extends Marketplace[F, ItemMetadata] {

  private val F = implicitly[ConcurrentEffect[F]]

  private val grpcService: F[MarketplaceServiceFs2Grpc[F, Metadata]] =
    managedChannelBuilder.resource.allocated
      .map(tuple => MarketplaceServiceFs2Grpc.stub(tuple._1))

  override def runAuction(
    auction: Auction[ItemMetadata]
  ): fs2.Stream[F, BuyerNotification] =
    fs2.Stream
      .eval(grpcService)
      .flatMap(_.runAuction(auction, new Metadata()))
      .map(toNotification)
}
