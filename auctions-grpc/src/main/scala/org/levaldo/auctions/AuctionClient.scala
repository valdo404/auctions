package org.levaldo.auctions

import cats.effect.ExitCode
import io.grpc.netty.{NegotiationType, NettyChannelBuilder}
import monix.eval.{Task, TaskApp}
import org.levaldo.auctions.SpecificModel.{ItemMetadata, ItemType}

import scala.language.postfixOps

object AuctionClient extends TaskApp {
  override def run(args: List[String]): Task[ExitCode] = {
    import cats.implicits._
    import monix.eval._
    import cats.effect._

    import scala.concurrent.duration._

    val grpcChannel = NettyChannelBuilder
      .forAddress("127.0.0.1", 9999)
      .negotiationType(NegotiationType.PLAINTEXT)

    val mktClient = new GrpcMarketplace[Task](grpcChannel)

    mktClient
      .runAuction(
        new Auction[ItemMetadata](
          ItemMetadata("cheval", ItemType.Other),
          50,
          10 seconds
        )
      )
      .compile
      .toList
      .foreachL(println)
      .as(ExitCode.Success)
  }
}
