package org.levaldo.auctions

import com.google.protobuf.empty.Empty
import org.levaldo.auctions.SpecificModel.ItemMetadata
import pb.auctions
import pb.auctions.BuyerServiceFs2Grpc
import cats.effect._
import cats.implicits._

class GrpcBuyerServer[F[_]: ConcurrentEffect, A](
  buyerEndpoint: BuyerEndpoint[F, ItemMetadata]
) extends BuyerServiceFs2Grpc[F, A] {
  import PbConverters._

  val F = implicitly[ConcurrentEffect[F]]

  override def notify(request: auctions.Notification, ctx: A): F[Empty] =
    buyerEndpoint.notify(request) *> F.pure(Empty())

  override def bidOn(request: auctions.Auction,
                     ctx: A): fs2.Stream[F, auctions.Bid] = {
    buyerEndpoint.bidOn(request).map(fromBid)
  }
}
