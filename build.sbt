name := "auctions"

ThisBuild / organization := "org.levaldo.auctions"
ThisBuild / version := "0.1-SNAPSHOT"
ThisBuild / scalaVersion := "2.13.1"

Global / onChangedBuildSource := ReloadOnSourceChanges

// auctions tagless core engine
lazy val auctionsEngine = (project in file("auctions-engine"))

// core protobuf grammer
lazy val auctionsProtobuf =
  project
    .in(file("protobuf"))
    .enablePlugins(Fs2Grpc)

// auctions bidi grpc implementation with real item metadata
lazy val auctionsGrpc =
  (project in file("auctions-grpc"))
    .dependsOn(auctionsEngine, auctionsProtobuf)
    .enablePlugins(JavaAppPackaging)
    .enablePlugins(DockerPlugin)

/**
  * todo: ui should create auctions, stream bids, and view auction result
  * todo: sangria, sbt-web, TS, vue js, bootstrap, apollo for UI layer
  */
lazy val root = (project in file("."))
  .aggregate(auctionsEngine, auctionsProtobuf, auctionsGrpc)
