libraryDependencies ++= Seq(
  "com.google.protobuf" % "protobuf-java" % "3.7.0" % "protobuf"
)

scalapbCodeGeneratorOptions += CodeGeneratorOption.FlatPackage
PB.protoSources in Compile += target.value / "protobuf_external"
