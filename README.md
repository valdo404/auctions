## Full scala auction engine.

### It comes in three modules:

* `auction-engine` which basically contains all the business stuff like:
  - Full tagless generic data model: Marketplace, Auction, Bid, etc
  - Marketplace second price implementation
  - Unit tests
  - BuyerRegistry

* `auction-grpc` which is a deployment-ready implementation of auction engine
   It is build around grpc for inter process communication.
   It also has a full bid and auction meta model.
   Execution is done by monix. And it has full reflection support.

* `protobuf` which is the recipient for generated PB and Grpc-FS2 code.

### How to run:

* Tests: 

   `sbt test`
   
* Program: 

   `sbt "auctionsGrpc/runMain org.levaldo.auctions.AuctionApps"` that will launch a marketplace and two buyers
   
   And in another tab: `sbt "auctionsGrpc/runMain org.levaldo.auctions.AuctionClient"` that will run some auction
   
* How to build

    `sbt "docker:publishLocal"`

### How to develop with:

Please use intellij with sbt shell integration, otherwise PB grammar won't be generated.

### TODO

Next updates will be a etcd integration + autodiscovery + vue js frontend + kafka integration + graphql + sql persistence layer
