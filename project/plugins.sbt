addSbtPlugin("org.lyranthe.fs2-grpc" % "sbt-java-gen" % "0.5.2")
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.4.1")
addSbtPlugin("com.thesamet" % "sbt-protoc" % "0.99.25")
